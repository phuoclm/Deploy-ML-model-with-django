import pytest
"""
    This test case is created for the purpose of performing the demo tool CircleCI.
    No effect in testing the project.
"""
def func(x):
    return x + 5
def test():
    assert func(4) == 9

